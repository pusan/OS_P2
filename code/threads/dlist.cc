#include "dlist.h"

extern void addItem(int n, DLList* list);
extern void reMove(int n, DLList* list);

DLLElement::DLLElement(void *itemPtr, int sortKey)
{
	left = NULL;
	right = NULL;
	key = sortKey;
	item = itemPtr;
}

void DLList::HW1Test()
{
	DLList* list = new DLList();
	addItem(19, list);
	reMove(19, list);
}
DLList::DLList() 
{
	first = NULL;
	last = NULL;
}
DLList::~DLList()
{
	DLLElement* newEl = first;
	while(true)
	{
		if (newEl == NULL)
			return;
		delete newEl;
		newEl = newEl->right;
	}
}
// initialize a list element

void DLList::Prepend(void *item)
{
	DLLElement* newEl;
	
	if (!IsEmpty())
	{
		newEl = new DLLElement(item, START_NUM);
		last = newEl;
	}
	else
	{
		newEl = new DLLElement(item, first->key-1);
		newEl->right = first;
		first->left = newEl;
	}
	first = newEl;
}

// add to head of list (set key = min_key-1) 
void DLList::Append(void *item) 
{
	DLLElement* newEl;

	if (!IsEmpty()){

		newEl = new DLLElement(item, START_NUM);
		first = newEl;

	}
	else
	{
		newEl = new DLLElement(item, last->key+1);
		newEl->left = last;
		last->right = newEl;
	}
	last = newEl;
} 
// add to tail of list (set key = max_key+1) 
void* DLList::Remove(int* keyPtr) {
	if (!IsEmpty())
		return NULL;

	DLLElement* removeNd = first;
	*keyPtr = removeNd->key;
	first = first->right;
	
	return removeNd->item;
}  
// remove from head of list
// set *keyPtr to key of the removed item
// return item (or NULL if list is empty)

bool DLList::IsEmpty() {
	if(first == NULL)
		return false;
	else
		return true;
}             // return true if list has elements
							// routines to put/get items on/off list in order (sorted by key)

void DLList::SortedInsert(void *item, int sortKey) {
	
	DLLElement* firstNode = first;
	DLLElement* newEl = new DLLElement(item, sortKey);
	
	if (firstNode == NULL) // list is empty
	{	
		first = newEl;
		last = newEl;
		return;
	}	
	else if (sortKey <= firstNode->key) //insert at first
	{
		first = newEl;
		first->right = firstNode;
		firstNode->left = first;
		return;
	}
	else
	{
		for(DLLElement* curNode = firstNode; curNode != NULL; curNode = curNode->right)
		{
			if (sortKey <= curNode->key)
			{
				DLLElement* prevNode = curNode->left;
				
				prevNode->right = newEl;
				curNode->left = newEl;
				
				newEl->left = prevNode;
				newEl->right = curNode;
				return;
			}
		}

		last->right = newEl;
		newEl->left = last;
		last = newEl;
	}
}
void* DLList::SortedRemove(int sortKey) {
	DLLElement* firstNode = first;

	if (firstNode == NULL)
		return NULL;

	do{
		if (firstNode->key == sortKey)
		{
			//remove
			if (firstNode->left == NULL) // node is first node
			{
				first = firstNode->right;
				first->left = NULL;
			}
			else if (firstNode == last) // node is last node.
			{
				last->right = NULL;
				last = firstNode->left;
			}
			else
			{
				firstNode->right->left = firstNode->left;
				firstNode->left->right = firstNode->right;
			}

			return firstNode;
		}
		
	} while ((firstNode = firstNode->right) != NULL);

	return NULL; 
}  // remove first item with key==sortKey
